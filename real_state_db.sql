USE [master]
GO
/****** Object:  Database [real_state_db]    Script Date: 1/23/2022 3:00:47 PM ******/
CREATE DATABASE [real_state_db]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'real_state_db', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\real_state_db.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'real_state_db_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\real_state_db_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [real_state_db] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [real_state_db].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [real_state_db] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [real_state_db] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [real_state_db] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [real_state_db] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [real_state_db] SET ARITHABORT OFF 
GO
ALTER DATABASE [real_state_db] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [real_state_db] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [real_state_db] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [real_state_db] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [real_state_db] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [real_state_db] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [real_state_db] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [real_state_db] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [real_state_db] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [real_state_db] SET  ENABLE_BROKER 
GO
ALTER DATABASE [real_state_db] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [real_state_db] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [real_state_db] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [real_state_db] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [real_state_db] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [real_state_db] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [real_state_db] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [real_state_db] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [real_state_db] SET  MULTI_USER 
GO
ALTER DATABASE [real_state_db] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [real_state_db] SET DB_CHAINING OFF 
GO
ALTER DATABASE [real_state_db] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [real_state_db] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [real_state_db] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [real_state_db] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [real_state_db] SET QUERY_STORE = OFF
GO
USE [real_state_db]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 1/23/2022 3:00:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Owner]    Script Date: 1/23/2022 3:00:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Owner](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Address] [nvarchar](max) NOT NULL,
	[Photo] [nvarchar](max) NULL,
	[Birthday] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Owner] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Property]    Script Date: 1/23/2022 3:00:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Address] [nvarchar](max) NOT NULL,
	[Price] [float] NOT NULL,
	[CodeInternal] [nvarchar](max) NULL,
	[Year] [int] NOT NULL,
	[IdOwner] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Property] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PropertyImage]    Script Date: 1/23/2022 3:00:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PropertyImage](
	[Id] [uniqueidentifier] NOT NULL,
	[IdProperty] [uniqueidentifier] NOT NULL,
	[File] [nvarchar](max) NOT NULL,
	[Enabled] [bit] NULL,
 CONSTRAINT [PK_PropertyImage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PropertyTrace]    Script Date: 1/23/2022 3:00:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PropertyTrace](
	[Id] [uniqueidentifier] NOT NULL,
	[DateSale] [datetime2](7) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Value] [float] NOT NULL,
	[Tax] [float] NOT NULL,
	[IdProperty] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_PropertyTrace] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 1/23/2022 3:00:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](450) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [IX_Property_IdOwner]    Script Date: 1/23/2022 3:00:47 PM ******/
CREATE NONCLUSTERED INDEX [IX_Property_IdOwner] ON [dbo].[Property]
(
	[IdOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_PropertyImage_IdProperty]    Script Date: 1/23/2022 3:00:47 PM ******/
CREATE NONCLUSTERED INDEX [IX_PropertyImage_IdProperty] ON [dbo].[PropertyImage]
(
	[IdProperty] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_PropertyTrace_IdProperty]    Script Date: 1/23/2022 3:00:47 PM ******/
CREATE NONCLUSTERED INDEX [IX_PropertyTrace_IdProperty] ON [dbo].[PropertyTrace]
(
	[IdProperty] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Users_UserName]    Script Date: 1/23/2022 3:00:47 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Users_UserName] ON [dbo].[Users]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PropertyImage] ADD  DEFAULT (CONVERT([bit],(1))) FOR [Enabled]
GO
ALTER TABLE [dbo].[Property]  WITH CHECK ADD  CONSTRAINT [FK_Property_Owner_IdOwner] FOREIGN KEY([IdOwner])
REFERENCES [dbo].[Owner] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Property] CHECK CONSTRAINT [FK_Property_Owner_IdOwner]
GO
ALTER TABLE [dbo].[PropertyImage]  WITH CHECK ADD  CONSTRAINT [FK_PropertyImage_Property_IdProperty] FOREIGN KEY([IdProperty])
REFERENCES [dbo].[Property] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PropertyImage] CHECK CONSTRAINT [FK_PropertyImage_Property_IdProperty]
GO
ALTER TABLE [dbo].[PropertyTrace]  WITH CHECK ADD  CONSTRAINT [FK_PropertyTrace_Property_IdProperty] FOREIGN KEY([IdProperty])
REFERENCES [dbo].[Property] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PropertyTrace] CHECK CONSTRAINT [FK_PropertyTrace_Property_IdProperty]
GO
USE [master]
GO
ALTER DATABASE [real_state_db] SET  READ_WRITE 
GO
