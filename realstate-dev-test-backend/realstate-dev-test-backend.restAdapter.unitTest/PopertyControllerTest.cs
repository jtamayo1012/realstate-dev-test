﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using realstate_dev_test_backend.application.unitTest.Mocks;
using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.domainApi.Port;
using realstate_dev_test_backend.domainApi.Services;
using realstate_dev_test_backend.restAdapter.Controllers.v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.restAdapter.unitTest
{
    public class PopertyControllerTest
    {
        private PropertyController _controller;
        private Mock<IRequestProperty<Property, PropertyFilters>> _mockApplication;
        private Mock<ILogger<PropertyController>> _mockLogger;

        [SetUp]
        public void Setup()
        {
            _mockApplication = new Mock<IRequestProperty<Property, PropertyFilters>>();
            _mockLogger = new Mock<ILogger<PropertyController>>();
        }

        [Test]
        public void TestGetAll_Ok()
        {
            _mockApplication.Setup(m => m.GetProperties()).Returns(Task<List<Property>>.Factory.StartNew(() => PropertyRepositoryMock.propertiesMock));

            _controller = new PropertyController(_mockApplication.Object, _mockLogger.Object);
            Task<IActionResult> actionResult = _controller.Get();
            actionResult.Wait();
            OkObjectResult okResult = actionResult.Result as OkObjectResult;
            List<Property> result = okResult.Value as List<Property>;
            Assert.LessOrEqual(0, result.Count());
        }

        [Test]
        public void TestGetFilters_Ok()
        {
            _mockApplication.Setup(m => m.FilterProperties(It.IsAny<PropertyFilters>())).Returns(Task<List<Property>>.Factory.StartNew(() => PropertyRepositoryMock.propertiesMock));

            _controller = new PropertyController(_mockApplication.Object, _mockLogger.Object);
            Task<IActionResult> actionResult = _controller.GetByFilters(PropertyRepositoryMock.filterMock);
            actionResult.Wait();
            OkObjectResult okResult = actionResult.Result as OkObjectResult;
            List<Property> result = okResult.Value as List<Property>;
            Assert.LessOrEqual(0, result.Count());
        }

        [Test]
        public void TestPost_Ok()
        {
            _mockApplication.Setup(m => m.AddProperty(It.IsAny<Property>())).Returns(Task<Property>.Factory.StartNew(() => PropertyRepositoryMock.propertyMock));

            _controller = new PropertyController(_mockApplication.Object, _mockLogger.Object);
            Task<IActionResult> actionResult = _controller.Post(PropertyRepositoryMock.propertyMock);
            actionResult.Wait();
            OkObjectResult okResult = actionResult.Result as OkObjectResult;
            Property result = okResult.Value as Property;
            Assert.LessOrEqual("test", result.Name);
        }


        [Test]
        public void TestChangePrice_Ok()
        {
            _mockApplication.Setup(m => m.ChangePriceProperty(It.IsAny<Guid>(), 1200)).Returns(Task<Property>.Factory.StartNew(() => PropertyRepositoryMock.propertyMock));

            _controller = new PropertyController(_mockApplication.Object, _mockLogger.Object);
            Task<IActionResult> actionResult = _controller.ChangePrice(Guid.NewGuid(), 1200);
            actionResult.Wait();
            OkObjectResult okResult = actionResult.Result as OkObjectResult;
            Property result = okResult.Value as Property;
            Assert.LessOrEqual("test", result.Name);
        }

        [Test]
        public void TestUpdate_Ok()
        {
            _mockApplication.Setup(m => m.UpdateProperty(It.IsAny<Guid>(), It.IsAny<Property>())).Returns(Task<Property>.Factory.StartNew(() => PropertyRepositoryMock.propertyMock));

            _controller = new PropertyController(_mockApplication.Object, _mockLogger.Object);
            Task<IActionResult> actionResult = _controller.Put(Guid.NewGuid(), PropertyRepositoryMock.propertyMock);
            actionResult.Wait();
            OkObjectResult okResult = actionResult.Result as OkObjectResult;
            Property result = okResult.Value as Property;
            Assert.LessOrEqual("test", result.Name);
        }
    }
}
