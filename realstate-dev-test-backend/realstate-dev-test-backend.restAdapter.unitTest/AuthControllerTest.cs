﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using realstate_dev_test_backend.domainApi.Port;
using realstate_dev_test_backend.domainApi.Services;
using realstate_dev_test_backend.restAdapter.Controllers.v1;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.restAdapter.unitTest
{
    public class AuthControllerTest
    {
        private AuthController _controller;
        private Mock<IRequestAuth<string, UserLogin>> _mockApplication;
        private Mock<ILogger<AuthController>> _mockLogger;

        [SetUp]
        public void Setup()
        {
            _mockApplication = new Mock<IRequestAuth<string, UserLogin>>();
            _mockLogger = new Mock<ILogger<AuthController>>();
        }

        [Test]
        public void TestPost_Ok()
        {
            UserLogin mockCredential = new UserLogin()
            {
                UserName = "test",
                Password = "test"
            };
            _mockApplication.Setup(m => m.Login(It.IsAny<UserLogin>())).Returns(Task<string>.Factory.StartNew(() => "test token"));

            _controller = new AuthController(_mockApplication.Object, _mockLogger.Object);
            Task<IActionResult> actionResult = _controller.Post(mockCredential);
            actionResult.Wait();
            OkObjectResult okResult = actionResult.Result as OkObjectResult;
            string result = okResult.Value as string;
            Assert.LessOrEqual("test token", result);
        }
    }
}
