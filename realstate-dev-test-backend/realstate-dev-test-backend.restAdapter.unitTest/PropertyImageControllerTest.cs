﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.domainApi.Port;
using realstate_dev_test_backend.restAdapter.Controllers.v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.restAdapter.unitTest
{
    public class PropertyImageControllerTest
    {
        private PropertyImageController _controller;
        private Mock<IRequestPropertyImage<PropertyImage>> _mockApplication;
        private Mock<IFileAttachment<IFormFile>> _mockFileService;
        private Mock<ILogger<PropertyImageController>> _mockLogger;

        [SetUp]
        public void Setup()
        {
            _mockApplication = new Mock<IRequestPropertyImage<PropertyImage>>();
            _mockFileService = new Mock<IFileAttachment<IFormFile>>();
            _mockLogger = new Mock<ILogger<PropertyImageController>>();
        }

        [Test]
        public void TestUploadImage_Ok()
        {
            PropertyImage mockImage = new PropertyImage()
            {
                Id = Guid.NewGuid(),
                File = "file",
                Enabled = true,
                IdProperty = Guid.NewGuid()
            };
            
            _mockFileService.Setup(m => m.UploadFile(It.IsAny<IFormFile>())).Returns("path");
            _mockApplication.Setup(m => m.AddPropertyImage(It.IsAny<Guid>(), It.IsAny<string>())).Returns(Task<PropertyImage>.Factory.StartNew(() => mockImage));

            _controller = new PropertyImageController(_mockApplication.Object, _mockFileService.Object, _mockLogger.Object);
            Task<IActionResult> actionResult = _controller.UploadImage(It.IsAny<Guid>(), It.IsAny<IFormFile>());
            actionResult.Wait();
            OkObjectResult okResult = actionResult.Result as OkObjectResult;
            PropertyImage result = okResult.Value as PropertyImage;
            Assert.AreEqual("file", result.File);
        }
    }
}
