﻿using Microsoft.EntityFrameworkCore;
using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.domainApi.Port;
using realstsate_dev_test_backend.persistenceAdapter.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.persistenceAdapter.Repositories
{
    public class PropertyRespository : IPropertyRepository<Property>
    {
        private readonly ApplicationDbContext _context;

        public PropertyRespository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Property> Add(Property property)
        {
            await _context.Properties.AddAsync(property);
            await _context.SaveChangesAsync();
            return property;
        }

        public async Task<List<Property>> GetAll()
        {
            return await _context.Properties.ToListAsync();
        }

        public async Task<Property> GetById(Guid id)
        {
            return await _context.Properties.FindAsync(id);
        }

        public async Task<IQueryable<Property>> GetQueryable()
        {
            return _context.Properties.AsQueryable();
        }

        public async Task<Property> Update(Property newData)
        {
            _context.Properties.Update(newData);
            await _context.SaveChangesAsync();
            return newData;
        }
    }
}
