﻿using Microsoft.EntityFrameworkCore;
using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.domainApi.Port;
using realstsate_dev_test_backend.persistenceAdapter.Context;
using System.Linq;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.persistenceAdapter.Repositories
{
    public class UserRepository : IUserRepository<User>
    {
        private readonly ApplicationDbContext _context;

        public UserRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<User> GetUserByUserName(string userName)
        {
            return await _context.Users.Where(u => u.UserName.Equals(userName)).SingleOrDefaultAsync();
        }
    }
}
