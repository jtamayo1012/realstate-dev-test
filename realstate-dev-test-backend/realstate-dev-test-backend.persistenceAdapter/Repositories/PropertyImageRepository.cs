﻿using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.domainApi.Port;
using realstsate_dev_test_backend.persistenceAdapter.Context;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.persistenceAdapter.Repositories
{
    public class PropertyImageRepository : IPropertyImageRepository<PropertyImage>
    {
        private readonly ApplicationDbContext _context;

        public PropertyImageRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<PropertyImage> Add(PropertyImage propertyImage)
        {
            await _context.PropertyImages.AddAsync(propertyImage);
            await _context.SaveChangesAsync();
            return propertyImage;
        }
    }
}
