﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.domainApi.Services;
using System;

namespace realstate_dev_test_backend.persistenceAdapter.Config
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");
            builder.HasKey(c => c.Id);

            builder.Property(p => p.UserName).IsRequired();
            builder.HasIndex(i => i.UserName).IsUnique();
            builder.Property(p => p.Password).IsRequired();

            //default user
            builder.HasData(new User { Id = Guid.NewGuid(), UserName = "admin", Password = Encrypt.MD5("admin") });
        }
    }
}
