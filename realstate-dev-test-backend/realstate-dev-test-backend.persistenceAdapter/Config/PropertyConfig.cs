﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using realstate_dev_test_backend.domainApi.Model;

namespace realstate_dev_test_backend.persistenceAdapter.Config
{
    public class PropertyConfig : IEntityTypeConfiguration<Property>
    {
        public void Configure(EntityTypeBuilder<Property> builder)
        {
            builder.ToTable("Property");
            builder.HasKey(c => c.Id);

            builder.Property(p => p.Name).IsRequired();
            builder.Property(p => p.Address).IsRequired();
            builder.Property(p => p.Price).IsRequired();

            builder.HasOne(o => o.Owner).WithMany(p => p.Properties).HasForeignKey(f => f.IdOwner);
            
        }
    }
}
