﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using realstate_dev_test_backend.domainApi.Model;

namespace realstate_dev_test_backend.persistenceAdapter.Config
{
    public class PropertyTraceConfig : IEntityTypeConfiguration<PropertyTrace>
    {
        public void Configure(EntityTypeBuilder<PropertyTrace> builder)
        {
            builder.ToTable("PropertyTrace");
            builder.HasKey(c => c.Id);

            builder.Property(p => p.Name).IsRequired();
            builder.Property(p => p.DateSale).IsRequired();
            builder.Property(p => p.Value).IsRequired();
            builder.Property(p => p.Tax).IsRequired();

            builder.HasOne(p => p.Property).WithMany(p => p.Tracking).HasForeignKey(f => f.IdProperty);

        }
    }
}
