﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using realstate_dev_test_backend.domainApi.Model;

namespace realstate_dev_test_backend.persistenceAdapter.Config
{
    public class OwnerConfig : IEntityTypeConfiguration<Owner>
    {
        public void Configure(EntityTypeBuilder<Owner> builder)
        {
            builder.ToTable("Owner");
            builder.HasKey(c => c.Id);

            builder.Property(p => p.Name).IsRequired();
            builder.Property(p => p.Address).IsRequired();
        }
    }
}
