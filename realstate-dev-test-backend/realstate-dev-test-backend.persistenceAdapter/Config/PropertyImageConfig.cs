﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using realstate_dev_test_backend.domainApi.Model;

namespace realstate_dev_test_backend.persistenceAdapter.Config
{
    public class PropertyImageConfig : IEntityTypeConfiguration<PropertyImage>
    {
        public void Configure(EntityTypeBuilder<PropertyImage> builder)
        {
            builder.ToTable("PropertyImage");
            builder.HasKey(c => c.Id);

            builder.Property(p => p.File).IsRequired();
            builder.Property(p => p.Enabled).HasDefaultValue(true);

            builder.HasOne(p => p.Property).WithMany(p => p.Images).HasForeignKey(f => f.IdProperty);
        }
    }
}
