﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.domainApi.Port;
using realstate_dev_test_backend.persistenceAdapter.Repositories;
using realstsate_dev_test_backend.persistenceAdapter.Context;

namespace realstate_dev_test_backend.persistenceAdapter
{
    public static class PersistenceExtension
    {
        public static void AddPersistence(this IServiceCollection serviceCollection, string connectionString)
        {
            serviceCollection.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(connectionString));
            serviceCollection.AddScoped<IPropertyRepository<Property>, PropertyRespository>();
            serviceCollection.AddScoped<IPropertyImageRepository<PropertyImage>, PropertyImageRepository>();
            serviceCollection.AddScoped<IUserRepository<User>, UserRepository>();
        }
    }
}
