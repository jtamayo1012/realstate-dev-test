﻿using Microsoft.EntityFrameworkCore;
using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.persistenceAdapter.Config;

namespace realstsate_dev_test_backend.persistenceAdapter.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Owner> Owners { get; set; }
        public DbSet<PropertyImage> PropertyImages { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<PropertyTrace> PropertyTraces { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new PropertyConfig());
            builder.ApplyConfiguration(new OwnerConfig());
            builder.ApplyConfiguration(new PropertyImageConfig());
            builder.ApplyConfiguration(new PropertyTraceConfig());
            builder.ApplyConfiguration(new UserConfig());
        }

    }
}
