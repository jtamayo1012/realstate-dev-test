﻿using NUnit.Framework;
using realstate_dev_test_backend.domainApi.Model;
using System;

namespace realstate_dev_test_backend.domainApi.unitTest
{
    public class PropertyTraceTest
    {
        private readonly PropertyTrace _propertyTrace;
        private readonly string Name = "test";
        private readonly double Value = 1000;
        private readonly double Tax = 0.1;
        private readonly Guid IdProperty = Guid.NewGuid();

        public PropertyTraceTest()
        {
            _propertyTrace = new PropertyTrace();
        }

        [Test]
        public void TestSetAndGetName()
        {
            _propertyTrace.Name = Name;
            Assert.AreEqual(Name, _propertyTrace.Name);
        }

        [Test]
        public void TestSetAndGetValue()
        {
            _propertyTrace.Value = Value;
            Assert.AreEqual(Value, _propertyTrace.Value);
        }

        [Test]
        public void TestSetAndGetTax()
        {
            _propertyTrace.Tax = Tax;
            Assert.AreEqual(Tax, _propertyTrace.Tax);
        }

        [Test]
        public void TestSetAndGetIdProperty()
        {
            _propertyTrace.IdProperty = IdProperty;
            Assert.AreEqual(IdProperty, _propertyTrace.IdProperty);
        }
    }
}
