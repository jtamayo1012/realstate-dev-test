﻿using NUnit.Framework;
using realstate_dev_test_backend.domainApi.Model;
using System;

namespace realstate_dev_test_backend.domainApi.unitTest
{
    public class PropertyTest
    {
        private readonly Property _property;
        private readonly string Name = "test";
        private readonly string Address = "test";
        private readonly double Price = 1200;
        private readonly string CodeInternal = "test";
        private readonly int Year = 2020;
        private readonly Guid IdOwner = Guid.NewGuid();

        public PropertyTest()
        {
            _property = new Property();
        }

        [Test]
        public void TestSetAndGetName()
        {
            _property.Name = Name;
            Assert.AreEqual(Name, _property.Name);
        }

        [Test]
        public void TestSetAndGetAddress()
        {
            _property.Address = Address;
            Assert.AreEqual(Address, _property.Address);
        }

        [Test]
        public void TestSetAndGetPrice()
        {
            _property.Price = Price;
            Assert.AreEqual(Price, _property.Price);
        }

        [Test]
        public void TestSetAndGetCodeInternal()
        {
            _property.CodeInternal = CodeInternal;
            Assert.AreEqual(CodeInternal, _property.CodeInternal);
        }

        [Test]
        public void TestSetAndGetYear()
        {
            _property.Year = Year;
            Assert.AreEqual(Year, _property.Year);
        }

        [Test]
        public void TestSetAndGetidOwner()
        {
            _property.IdOwner = IdOwner;
            Assert.AreEqual(IdOwner, _property.IdOwner);
        }
    }
}
