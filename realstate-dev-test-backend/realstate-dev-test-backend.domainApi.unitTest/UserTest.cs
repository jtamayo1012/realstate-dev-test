﻿using NUnit.Framework;
using realstate_dev_test_backend.domainApi.Model;

namespace realstate_dev_test_backend.domainApi.unitTest
{
    public class UserTest
    {
        private readonly User _user;
        private readonly string UserName = "test";
        private readonly string Password = "test";

        public UserTest()
        {
            _user = new User();
        }

        [Test]
        public void TestSetAndGetName()
        {
            _user.UserName = UserName;
            Assert.AreEqual(UserName, _user.UserName);
        }

        [Test]
        public void TestSetAndGetAddress()
        {
            _user.Password = Password;
            Assert.AreEqual(Password, _user.Password);
        }
    }
}
