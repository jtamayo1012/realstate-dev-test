﻿using NUnit.Framework;
using realstate_dev_test_backend.domainApi.Model;

namespace realstate_dev_test_backend.domainApi.unitTest
{
    public class PropertyImageTest
    {
        private readonly PropertyImage _propertyImage;
        private readonly string File = "test";
        private readonly bool Enabled = true;
        public PropertyImageTest()
        {
            _propertyImage = new PropertyImage();
        }

        [Test]
        public void TestSetAndGetFile()
        {
            _propertyImage.File = File;
            Assert.AreEqual(File, _propertyImage.File);
        }

        [Test]
        public void TestSetAndGetEnabled()
        {
            _propertyImage.Enabled = Enabled;
            Assert.AreEqual(Enabled, _propertyImage.Enabled);
        }
    }
}
