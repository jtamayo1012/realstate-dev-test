﻿using NUnit.Framework;
using realstate_dev_test_backend.domainApi.Model;
using System;

namespace realstate_dev_test_backend.domainApi.unitTest
{
    public class OwnerTest
    {
        private readonly Owner _owner;
        private readonly string Name = "test";
        private readonly string Address = "test";
        private readonly string Photo = "test";
        private readonly DateTime Birthday = new DateTime();

        public OwnerTest()
        {
            _owner = new Owner();
        }

        [Test]
        public void TestSetAndGetName()
        {
            _owner.Name = Name;
            Assert.AreEqual(Name, _owner.Name);
        }

        [Test]
        public void TestSetAndGetAddress()
        {
            _owner.Address = Address;
            Assert.AreEqual(Address, _owner.Address);
        }

        [Test]
        public void TestSetAndGetPhoto()
        {
            _owner.Photo = Photo;
            Assert.AreEqual(Photo, _owner.Photo);
        }

        [Test]
        public void TestSetAndGetBirthday()
        {
            _owner.Birthday = Birthday;
            Assert.AreEqual(Birthday, _owner.Birthday);
        }
    }
}
