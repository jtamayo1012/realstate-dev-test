﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.domainApi.Port;
using realstate_dev_test_backend.domainApi.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.restAdapter.Controllers.v1
{
    [Authorize]
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class PropertyController : ControllerBase
    {
        private readonly IRequestProperty<Property, PropertyFilters> _requestProperty;
        private readonly ILogger<PropertyController> _logger;

        public PropertyController(IRequestProperty<Property, PropertyFilters> requestProperty, ILogger<PropertyController> logger)
        {
            _requestProperty = requestProperty;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await _requestProperty.GetProperties();
            return Ok(result); 
        }

        [HttpGet("filter")]
        public async Task<IActionResult> GetByFilters([FromQuery] PropertyFilters filters)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                List<Property> newProperty = await _requestProperty.FilterProperties(filters);
                return Ok(newProperty);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return StatusCode(500, "Unexpected error!");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Property property)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                Property newProperty = await _requestProperty.AddProperty(property);
                return Ok(newProperty);
            }
            catch(Exception e)
            {
                _logger.LogError(e.Message);
                return StatusCode(500, "Unexpected error!");
            }
        }

        [HttpPut("{id}/{newPrice}")]
        public async Task<IActionResult> ChangePrice(Guid id, double newPrice)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                Property newProperty = await _requestProperty.ChangePriceProperty(id, newPrice);
                return Ok(newProperty);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return StatusCode(500, "Unexpected error!");
            }
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] Property property)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                Property newProperty = await _requestProperty.UpdateProperty(id, property);
                return Ok(newProperty);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return StatusCode(500, "Unexpected error!");
            }
        }
    }
}
