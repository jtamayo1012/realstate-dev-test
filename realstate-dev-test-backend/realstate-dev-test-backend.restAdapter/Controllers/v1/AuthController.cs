﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using realstate_dev_test_backend.domainApi.Port;
using realstate_dev_test_backend.domainApi.Services;
using System;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.restAdapter.Controllers.v1
{
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IRequestAuth<string, UserLogin> _requestAuth;
        private readonly ILogger<AuthController> _logger;

        public AuthController(IRequestAuth<string, UserLogin> requestAuth, ILogger<AuthController> logger)
        {
            _requestAuth = requestAuth;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserLogin user)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                string authToken = await _requestAuth.Login(user);
                return Ok(authToken);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return StatusCode(500, "Unexpected error!");
            }
        }
    }
}
