﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.domainApi.Port;
using System;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.restAdapter.Controllers.v1
{
    [Authorize]
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class PropertyImageController : ControllerBase
    {
        private readonly IRequestPropertyImage<PropertyImage> _requestPropertyImage;
        private readonly IFileAttachment<IFormFile> _fileService;
        private readonly ILogger<PropertyImageController> _logger;

        public PropertyImageController(IRequestPropertyImage<PropertyImage> requestPropertyImage, IFileAttachment<IFormFile> fileService, ILogger<PropertyImageController> logger)
        {
            _requestPropertyImage = requestPropertyImage;
            _fileService = fileService;
            _logger = logger;
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> UploadImage(Guid id, IFormFile image)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                string fileName = _fileService.UploadFile(image);
                PropertyImage newImage = await _requestPropertyImage.AddPropertyImage(id, fileName);
                return Ok(newImage);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return StatusCode(500, "Unexpected error!");
            }
        }
    }
}
