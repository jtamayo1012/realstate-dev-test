﻿using System.ComponentModel.DataAnnotations;

namespace realstate_dev_test_backend.domainApi
{
    public class BaseEntity<TKey>
    {
        [Key]
        public TKey Id { get; set; }
    }
}
