﻿using System;

namespace realstate_dev_test_backend.domainApi.Model
{
    public class User : BaseEntity<Guid>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
