﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace realstate_dev_test_backend.domainApi.Model
{
    public class Property : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public double Price { get; set; }
        public string CodeInternal { get; set; }
        public int Year { get; set; }
        public Guid IdOwner { get; set; }

        [JsonIgnore]
        public Owner Owner { get; set; }
        [JsonIgnore]
        public List<PropertyImage> Images { get; set; }
        [JsonIgnore]
        public List<PropertyTrace> Tracking { get; set; }
    }
}
