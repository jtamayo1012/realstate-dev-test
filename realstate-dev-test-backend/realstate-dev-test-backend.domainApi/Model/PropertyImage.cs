﻿using System;
using System.Text.Json.Serialization;

namespace realstate_dev_test_backend.domainApi.Model
{
    public class PropertyImage : BaseEntity<Guid>
    {
        public Guid IdProperty { get; set; }
        public string File { get; set; }
        public bool? Enabled { get; set; }

        [JsonIgnore]
        public Property Property { get; set; }
    }
}
