﻿using System;
using System.Text.Json.Serialization;

namespace realstate_dev_test_backend.domainApi.Model
{
    public class PropertyTrace : BaseEntity<Guid>
    {
        public DateTime DateSale { get; set; }
        public string Name { get; set; }
        public double Value { get; set; }
        public double Tax { get; set; }
        public Guid IdProperty { get; set; }

        [JsonIgnore]
        public Property Property { get; set; }
    }
}
