﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace realstate_dev_test_backend.domainApi.Model
{
    public class Owner : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Photo { get; set; }
        public DateTime Birthday { get; set; }

        [JsonIgnore]
        public List<Property> Properties { get; set; }
    }
}
