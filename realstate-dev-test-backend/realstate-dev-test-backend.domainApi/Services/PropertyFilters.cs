﻿
namespace realstate_dev_test_backend.domainApi.Services
{
    public class PropertyFilters : BaseFilters
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public double? initialPrice { get; set; }
        public double? finalPrice { get; set; }
        public int? Year { get; set; }
    }
}
