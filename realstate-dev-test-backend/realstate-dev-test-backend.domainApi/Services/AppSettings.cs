﻿

namespace realstate_dev_test_backend.domainApi.Services
{
    public class AppSettings
    {
        public string ApplicationName { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string JwtSecretKey { get; set; } = string.Empty;
    }
}
