﻿

namespace realstate_dev_test_backend.domainApi.Services
{
    public class UserLogin
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
