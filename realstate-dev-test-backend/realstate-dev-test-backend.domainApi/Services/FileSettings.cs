﻿

namespace realstate_dev_test_backend.domainApi.Services
{
    public static class FileSettings
    {
        public static string[] IMAGE_FORMATS_VALID = new string[] { ".png", ".PNG" };
    }
}
