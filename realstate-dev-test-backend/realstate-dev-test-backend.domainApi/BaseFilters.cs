﻿
namespace realstate_dev_test_backend.domainApi
{
    public class BaseFilters
    {
        public int? Page { get; set; }
        public string Sort { get; set; }
        public int Limit { get; set; } = 5;
        public string Dir { get; set; } = "asc";
    }
}
