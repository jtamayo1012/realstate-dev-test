﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.domainApi.Port
{
    public interface IPropertyRepository<T>
    {
        Task<List<T>> GetAll();
        Task<T> Add(T property);
        Task<T> Update(T newData);
        Task<T> GetById(Guid id);
        Task<IQueryable<T>> GetQueryable();
    }
}
