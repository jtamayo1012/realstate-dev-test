﻿using System.Threading.Tasks;

namespace realstate_dev_test_backend.domainApi.Port
{
    public interface IRequestAuth<T, R>
    {
        Task<T> Login(R model);
    }
}
