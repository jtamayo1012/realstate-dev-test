﻿using System.Threading.Tasks;

namespace realstate_dev_test_backend.domainApi.Port
{
    public interface IFileAttachment<T>
    {
        string UploadFile(T file);
    }
}
