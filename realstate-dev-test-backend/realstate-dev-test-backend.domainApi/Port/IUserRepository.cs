﻿using System.Threading.Tasks;

namespace realstate_dev_test_backend.domainApi.Port
{
    public interface IUserRepository<T>
    {
        Task<T> GetUserByUserName(string userName);
    }
}
