﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.domainApi.Port
{
    public interface IRequestProperty<T, K>
    {
        Task<List<T>> GetProperties();
        Task<T> AddProperty(T property);
        Task<T> ChangePriceProperty(Guid id, double newPrice);
        Task<T> UpdateProperty(Guid id, T property);
        Task<List<T>> FilterProperties(K filters);
    }
}
