﻿using System;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.domainApi.Port
{
    public interface IRequestPropertyImage<T>
    {
        Task<T> AddPropertyImage(Guid idProperty, string filename);
    }
}
