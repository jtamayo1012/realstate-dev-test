﻿using System.Threading.Tasks;

namespace realstate_dev_test_backend.domainApi.Port
{
    public interface IPropertyImageRepository<T>
    {
        Task<T> Add(T propertyImage);
    }
}
