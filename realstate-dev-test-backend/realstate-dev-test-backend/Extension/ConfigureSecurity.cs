﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using realstate_dev_test_backend.domainApi.Services;
using System.Text;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.Extension
{
    public static class ConfigureSecurity
    {
        public static void AddJwtSecurity(this IServiceCollection serviceCollection, AppSettings appSettings)
        {
            serviceCollection.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
           .AddJwtBearer(options =>
           {
               options.TokenValidationParameters = new TokenValidationParameters
               {
                   ValidateIssuerSigningKey = true,
                   IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(appSettings.JwtSecretKey)),
                   ValidateIssuer = false,
                   ValidateAudience = false,
                   ValidateLifetime = true
               };
               options.Events = new JwtBearerEvents()
               {
                   OnAuthenticationFailed = context =>
                   {
                       if (context.Exception is SecurityTokenExpiredException)
                       {
                            // if you end up here, you know that the token is expired
                            context.Response.Headers.Add("Token-Expired", "true");
                       }
                       return Task.CompletedTask;
                   }
               };
           });

        }
    }
}
