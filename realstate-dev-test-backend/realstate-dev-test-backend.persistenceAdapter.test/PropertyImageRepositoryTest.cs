﻿using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.persistenceAdapter.Repositories;
using realstsate_dev_test_backend.persistenceAdapter.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.persistenceAdapter.test
{
    public class PropertyImageRepositoryTest
    {
        private PropertyImageRepository _repository;
        private readonly DbContextOptions<ApplicationDbContext> options = new DbContextOptionsBuilder<ApplicationDbContext>().UseInMemoryDatabase(databaseName: "testdb").Options;

        [SetUp]
        public void Setup()
        {
            using (var context = new ApplicationDbContext(options))
            {
                context.PropertyImages.Add(new PropertyImage()
                {
                    File = "test",
                    Enabled = true,
                    IdProperty = Guid.NewGuid()
                });
                context.SaveChanges();
            }
            _repository = new PropertyImageRepository(new ApplicationDbContext(options));
        }

        [Test]
        public void TestAdd_Ok()
        {
            PropertyImage newImage = new PropertyImage()
            {
                File = "test",
                Enabled = true,
                IdProperty = Guid.NewGuid()
            };
            Task<PropertyImage> result = _repository.Add(newImage);
            result.Wait();
            Assert.AreEqual("test", result.Result.File);
        }
    }
}
