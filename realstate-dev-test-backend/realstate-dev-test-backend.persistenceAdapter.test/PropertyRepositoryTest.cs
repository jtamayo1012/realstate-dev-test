﻿using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.persistenceAdapter.Repositories;
using realstsate_dev_test_backend.persistenceAdapter.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.persistenceAdapter.test
{
    public class PropertyRepositoryTest
    {
        private PropertyRespository _repository;
        private readonly DbContextOptions<ApplicationDbContext> options = new DbContextOptionsBuilder<ApplicationDbContext>().UseInMemoryDatabase(databaseName: "testdb").Options;

        [SetUp]
        public void Setup()
        {
            using (var context = new ApplicationDbContext(options))
            {
                context.Properties.Add(new Property() 
                { 
                    Name = "test",
                    Address = "test",
                    Price = 1200,
                    CodeInternal = "test",
                    Year = 2020,
                    IdOwner = Guid.NewGuid()
                });
                context.SaveChanges();
            }
            _repository = new PropertyRespository(new ApplicationDbContext(options));
        }

        [Test]
        public void TestGetIQueryable_Ok()
        {
            Task<IQueryable<Property>> result = _repository.GetQueryable();
            result.Wait();
            Assert.LessOrEqual(0, result.Result.ToList().Count());
        }

        [Test]
        public void TestGetById_Ok()
        {
            Task<List<Property>> all = _repository.GetAll();
            all.Wait();
            Task<Property> result = _repository.GetById(all.Result[0].Id);
            result.Wait();
            Assert.AreEqual("test", result.Result.Name);
        }

        [Test]
        public void TestUpdate_Ok()
        {
            Property updateProperty = new Property()
            {
                Name = "test update",
                Address = "test",
                Price = 1200,
                CodeInternal = "test",
                Year = 2020,
                IdOwner = Guid.NewGuid()
            };
            Task<Property> result = _repository.Update(updateProperty);
            result.Wait();
            Assert.AreEqual("test update", result.Result.Name);
        }

        [Test]
        public void TestAdd_Ok()
        {
            Property newProperty = new Property() 
            {
                Name = "test new",
                Address = "test",
                Price = 1200,
                CodeInternal = "test",
                Year = 2020,
                IdOwner = Guid.NewGuid()
            };
            Task<Property> result = _repository.Add(newProperty);
            result.Wait();
            Assert.AreEqual("test new", result.Result.Name);
        }

        [Test]
        public void TestGetAll_Ok()
        {
            Task<List<Property>> result = _repository.GetAll();
            result.Wait();
            Assert.LessOrEqual(0, result.Result.Count());
        }
    }
}
