﻿using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.persistenceAdapter.Repositories;
using realstsate_dev_test_backend.persistenceAdapter.Context;
using System;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.persistenceAdapter.test
{
    public class UserRespositoryTest
    {
        private UserRepository _repository;
        private readonly DbContextOptions<ApplicationDbContext> options = new DbContextOptionsBuilder<ApplicationDbContext>().UseInMemoryDatabase(databaseName: "testdb").Options;

        [SetUp]
        public void Setup()
        {
            using (var context = new ApplicationDbContext(options))
            {
                context.Users.Add(new User()
                {
                    Id = Guid.NewGuid(),
                    UserName = "test",
                    Password = "test"
                });
                context.SaveChanges();
            }
            _repository = new UserRepository(new ApplicationDbContext(options));
        }

        [Test]
        public void TestGetByUserNAme_Ok()
        {
            Task<User> result = _repository.GetUserByUserName("test");
            result.Wait();
            Assert.AreEqual("test", result.Result.UserName);
        }
    }
}
