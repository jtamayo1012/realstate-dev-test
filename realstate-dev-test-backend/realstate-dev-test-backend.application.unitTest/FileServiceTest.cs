﻿using Microsoft.AspNetCore.Http;
using Moq;
using NUnit.Framework;
using realstate_dev_test_backend.domainApi.Port;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.application.unitTest
{
    public class FileServiceTest
    {
        private IFileAttachment<IFormFile> _service;

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestUploadService_Ok()
        {
            Mock<IFormFile> imageMock = new Mock<IFormFile>();
            imageMock.Setup(m => m.Length).Returns(10);
            imageMock.Setup(m => m.FileName).Returns("test.png");
            imageMock.Setup(m => m.CopyTo(It.IsAny<FileStream>()));

            _service = new FileService<IFormFile>();
            string result = _service.UploadFile(imageMock.Object);
            Assert.NotNull(result);
        }
    }
}
