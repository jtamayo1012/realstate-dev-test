﻿using Moq;
using NUnit.Framework;
using realstate_dev_test_backend.application.unitTest.Mocks;
using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.domainApi.Port;
using System;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.application.unitTest
{
    public class PropertyImageServiceTest
    {
        private IRequestPropertyImage<PropertyImage> _service;
        private Mock<IPropertyRepository<Property>> _propertyRepositoryMock;
        private Mock<IPropertyImageRepository<PropertyImage>> _propertyRepositoryImageMock;

        [SetUp]
        public void Setup()
        {
            _propertyRepositoryMock = new Mock<IPropertyRepository<Property>>();
            _propertyRepositoryImageMock = new Mock<IPropertyImageRepository<PropertyImage>>();
        }

        [Test]
        public void TestAddProperty_Ok()
        {
            PropertyImage propertyImage = new PropertyImage()
            {
                Id = Guid.NewGuid(),
                File = "test",
                Enabled = true,
                IdProperty = Guid.NewGuid()
            };
            _propertyRepositoryMock.Setup(m => m.GetById(It.IsAny<Guid>())).Returns(Task<Property>.Factory.StartNew(() => PropertyRepositoryMock.propertyMock));
            _propertyRepositoryImageMock.Setup(m => m.Add(It.IsAny<PropertyImage>())).Returns(Task<PropertyImage>.Factory.StartNew(() => propertyImage));

            _service = new PropertyImageService<PropertyImage>(_propertyRepositoryImageMock.Object, _propertyRepositoryMock.Object);
            Task<PropertyImage> result = _service.AddPropertyImage(Guid.NewGuid(), "test");
            result.Wait();
            Assert.AreEqual("test", result.Result.File);
        }
    }
}
