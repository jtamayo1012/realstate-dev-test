﻿using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.domainApi.Services;
using System;
using System.Collections.Generic;

namespace realstate_dev_test_backend.application.unitTest.Mocks
{
    public static class PropertyRepositoryMock
    {
        public static Property propertyMock = new Property()
        {
            Id = Guid.NewGuid(),
            Name = "test",
            Address = "test",
            Price = 1200,
            CodeInternal = "test",
            Year = 2020,
            IdOwner = Guid.NewGuid()
        };

        public static List<Property> propertiesMock = new List<Property>()
        {
            new Property()
            {
                Id = Guid.NewGuid(),
                Name = "test",
                Address = "test",
                Price = 1200,
                CodeInternal = "test",
                Year = 2020,
                IdOwner = Guid.NewGuid()
            }
        };

        public static PropertyFilters filterMock = new PropertyFilters() 
        {
            Name = "test"
        };
    }
}
