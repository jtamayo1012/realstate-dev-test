﻿using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.domainApi.Port;
using realstate_dev_test_backend.domainApi.Services;
using System;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.application.unitTest
{
    public class AuthServiceTest
    {
        private IRequestAuth<string, UserLogin> _service;
        private Mock<IUserRepository<User>> _userRepositoryMock;
        private Mock<IOptions<AppSettings>> _optionsMock;

        [SetUp]
        public void Setup()
        {
            _userRepositoryMock = new Mock<IUserRepository<User>>();
            _optionsMock = new Mock<IOptions<AppSettings>>();
        }

        [Test]
        public void TestLogin_Ok()
        {
            User mockUser = new User()
            {
                Id = Guid.NewGuid(),
                UserName = "test",
                Password = "098f6bcd4621d373cade4e832627b4f6"
            };
            UserLogin mockCredential = new UserLogin()
            {
                UserName = "test",
                Password = "test"
            };
            AppSettings mockOptions = new AppSettings()
            {
                ApplicationName = "test",
                Description = "test",
                JwtSecretKey = "testdejwttokensecurity2022"
            };

            _userRepositoryMock.Setup(m => m.GetUserByUserName(It.IsAny<string>())).Returns(Task<User>.Factory.StartNew(() => mockUser));
            _optionsMock.Setup(m => m.Value).Returns(mockOptions);

            _service = new AuthService<string, UserLogin>(_userRepositoryMock.Object, _optionsMock.Object);
            Task<string> result = _service.Login(mockCredential);
            result.Wait();
            Assert.IsNotEmpty(result.Result);
        }
    }
}
