﻿using Moq;
using NUnit.Framework;
using realstate_dev_test_backend.application.unitTest.Mocks;
using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.domainApi.Port;
using realstate_dev_test_backend.domainApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.application.unitTest
{
    public class PropertyServiceTest
    {
        private IRequestProperty<Property, PropertyFilters> _service;
        private Mock<IPropertyRepository<Property>> _propertyRepositoryMock;

        [SetUp]
        public void Setup()
        {
            _propertyRepositoryMock = new Mock<IPropertyRepository<Property>>();
        }

        [Test]
        public void TestAddProperty_Ok()
        {
            _propertyRepositoryMock.Setup(m => m.Add(It.IsAny<Property>())).Returns(Task<Property>.Factory.StartNew(() => PropertyRepositoryMock.propertyMock));

            _service = new PropertyService<Property, PropertyFilters>(_propertyRepositoryMock.Object);
            Task<Property> result = _service.AddProperty(PropertyRepositoryMock.propertyMock);
            result.Wait();
            Assert.AreEqual("test", result.Result.Name);
        }

        [Test]
        public void TestChangePriceProperty_Ok()
        {
            _propertyRepositoryMock.Setup(m => m.GetById(It.IsAny<Guid>())).Returns(Task<Property>.Factory.StartNew(() => PropertyRepositoryMock.propertyMock));
            _propertyRepositoryMock.Setup(m => m.Update(It.IsAny<Property>())).Returns(Task<Property>.Factory.StartNew(() => PropertyRepositoryMock.propertyMock));

            _service = new PropertyService<Property, PropertyFilters>(_propertyRepositoryMock.Object);
            Task<Property> result = _service.ChangePriceProperty(Guid.NewGuid(), 1200);
            result.Wait();
            Assert.AreEqual(1200, result.Result.Price);
        }

        [Test]
        public void TestGetQueryableProperty_Ok()
        {
            _propertyRepositoryMock.Setup(m => m.GetQueryable()).Returns(Task<IQueryable<Property>>.Factory.StartNew(() => PropertyRepositoryMock.propertiesMock.AsQueryable()));

            _service = new PropertyService<Property, PropertyFilters>(_propertyRepositoryMock.Object);
            Task<List<Property>> result = _service.FilterProperties(PropertyRepositoryMock.filterMock);
            result.Wait();
            Assert.LessOrEqual(0, result.Result.Count());
        }

        [Test]
        public void TestGetAll_Ok()
        {
            _propertyRepositoryMock.Setup(m => m.GetAll()).Returns(Task<List<Property>>.Factory.StartNew(() => PropertyRepositoryMock.propertiesMock));

            _service = new PropertyService<Property, PropertyFilters>(_propertyRepositoryMock.Object);
            Task<List<Property>> result = _service.GetProperties();
            result.Wait();
            Assert.LessOrEqual(0, result.Result.Count());
        }

        [Test]
        public void TestUpdateProperty_Ok()
        {
            _propertyRepositoryMock.Setup(m => m.GetById(It.IsAny<Guid>())).Returns(Task<Property>.Factory.StartNew(() => PropertyRepositoryMock.propertyMock));
            _propertyRepositoryMock.Setup(m => m.Update(It.IsAny<Property>())).Returns(Task<Property>.Factory.StartNew(() => PropertyRepositoryMock.propertyMock));

            _service = new PropertyService<Property, PropertyFilters>(_propertyRepositoryMock.Object);
            Task<Property> result = _service.UpdateProperty(Guid.NewGuid(), PropertyRepositoryMock.propertyMock);
            result.Wait();
            Assert.AreEqual(1200, result.Result.Price);
        }
    }
}
