﻿using Microsoft.Extensions.DependencyInjection;
using realstate_dev_test_backend.domainApi.Port;

namespace realstate_dev_test_backend.application
{
    public static class ApplicationExtension
    {
        public static void AddApplication(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient(typeof(IRequestProperty<,>), typeof(PropertyService<,>));
            serviceCollection.AddTransient(typeof(IRequestPropertyImage<>), typeof(PropertyImageService<>));
            serviceCollection.AddTransient(typeof(IFileAttachment<>), typeof(FileService<>));
            serviceCollection.AddTransient(typeof(IRequestAuth<,>), typeof(AuthService<,>));
        }
    }
}
