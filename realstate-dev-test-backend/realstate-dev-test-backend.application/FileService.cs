﻿using Microsoft.AspNetCore.Http;
using realstate_dev_test_backend.domainApi.Port;
using realstate_dev_test_backend.domainApi.Services;
using System;
using System.IO;
using System.Linq;
using System.Text;

namespace realstate_dev_test_backend.application
{
    public class FileService<T> : IFileAttachment<IFormFile>
    {
        public string UploadFile(IFormFile file)
        {
            string path = $"{Directory.GetCurrentDirectory()}/Files";
            StringBuilder result = new StringBuilder();
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            if (file != null && file.Length > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var fileExtension = Path.GetExtension(fileName);
                if (FileSettings.IMAGE_FORMATS_VALID.Contains(fileExtension))
                {
                    var newFileName = String.Concat(Convert.ToString(Guid.NewGuid()), fileExtension);
                    result.Append(newFileName);
                    var filepath = Path.Combine(path, newFileName);

                    using (FileStream fs = File.Create(filepath))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                }
                else
                {
                    throw new Exception("invalid format!");
                }
                
            }
            else
            {
                throw new Exception("Image not found");
            }

            return result.ToString();
        }
    }
}
