﻿using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.domainApi.Port;
using System;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.application
{
    public class PropertyImageService<T> : IRequestPropertyImage<PropertyImage>
    {
        private readonly IPropertyImageRepository<PropertyImage> _propertyImageRepository;
        private readonly IPropertyRepository<Property> _propertyRepository;

        public PropertyImageService(IPropertyImageRepository<PropertyImage> propertyImageRepository, IPropertyRepository<Property> propertyRepository)
        {
            _propertyImageRepository = propertyImageRepository;
            _propertyRepository = propertyRepository;
        }

        public async Task<PropertyImage> AddPropertyImage(Guid idProperty, string filename)
        {
            Property query = await _propertyRepository.GetById(idProperty);

            if (query is null)
                throw new Exception("property not found!");

            PropertyImage newImage = new PropertyImage()
            {
                IdProperty = idProperty,
                File = filename,
                Enabled = true
            };
            return await _propertyImageRepository.Add(newImage);
        }
    }
}
