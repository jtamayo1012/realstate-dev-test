﻿using Microsoft.Extensions.Options;
using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.domainApi.Port;
using realstate_dev_test_backend.domainApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.application
{
    public class AuthService<T, R> : IRequestAuth<string, UserLogin>
    {
        private readonly IUserRepository<User> _repository;
        private readonly IOptions<AppSettings> _options;

        public AuthService(IUserRepository<User> repository, IOptions<AppSettings> options)
        {
            _repository = repository;
            _options = options;
        }

        public async Task<string> Login(UserLogin model)
        {
            User query = await _repository.GetUserByUserName(model.UserName);

            if (query is null)
                throw new Exception("User not found");

            string passEncrypt = Encrypt.MD5(model.Password);

            if(!query.Password.Equals(passEncrypt))
                throw new Exception("Unauthorized!");

            var claim = new[]
            {
                new Claim("UserName", query.UserName)
            };

            return JwtSecurity.GenerateToken(claim, _options.Value.JwtSecretKey);
        }
    }
}
