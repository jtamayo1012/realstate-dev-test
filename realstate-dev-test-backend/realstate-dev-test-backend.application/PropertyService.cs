﻿using realstate_dev_test_backend.domainApi.Model;
using realstate_dev_test_backend.domainApi.Port;
using realstate_dev_test_backend.domainApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace realstate_dev_test_backend.application
{
    public class PropertyService<T, K> : IRequestProperty<Property, PropertyFilters>
    {
        private readonly IPropertyRepository<Property> _repository;

        public PropertyService(IPropertyRepository<Property> repository)
        {
            _repository = repository;
        }

        public async Task<Property> AddProperty(Property property)
        {
            return await _repository.Add(property);
        }

        public async Task<Property> ChangePriceProperty(Guid id, double newPrice)
        {
            Property query = await _repository.GetById(id);
            if (query is null)
                throw new Exception("Not found");

            query.Price = newPrice;
            return await _repository.Update(query);
        }

        public async Task<List<Property>> FilterProperties(PropertyFilters filters)
        {
            IQueryable<Property> query = await _repository.GetQueryable();

            if (!string.IsNullOrWhiteSpace(filters.Name))
                query = query.Where(d => d.Name.Contains(filters.Name));
            if (!string.IsNullOrWhiteSpace(filters.Address))
                query = query.Where(d => d.Address == filters.Address);
            if (filters.initialPrice.HasValue && filters.finalPrice.HasValue)
                query = query.Where(d => d.Price >= filters.initialPrice && d.Price <= filters.finalPrice);
            if (filters.Year != null)
                query = query.Where(d => d.Year == filters.Year);

            switch (filters.Sort)
            {
                case "Name":
                    if (filters.Dir == "asc")
                        query = query.OrderBy(d => d.Name);
                    else if (filters.Dir == "desc")
                        query = query.OrderByDescending(d => d.Name);
                    break;
                case "Price":
                    if (filters.Dir == "asc")
                        query = query.OrderBy(d => d.Price);
                    else if (filters.Dir == "desc")
                        query = query.OrderByDescending(d => d.Price);
                    break;
                case "Year":
                    if (filters.Dir == "asc")
                        query = query.OrderBy(d => d.Year);
                    else if (filters.Dir == "desc")
                        query = query.OrderByDescending(d => d.Year);
                    break;
            }

            query = query.Take(filters.Limit);
            if (filters.Page.HasValue)
                query = query.Skip(filters.Page.Value * filters.Limit);

            return query.ToList();
        }

        public async Task<List<Property>> GetProperties()
        {
            return await _repository.GetAll();
        }

        public async Task<Property> UpdateProperty(Guid id, Property property)
        {
            Property query = await _repository.GetById(id);
            if (query is null)
                throw new Exception("Not found");

            query.Name = property.Name;
            query.Address = property.Address;
            query.CodeInternal = property.CodeInternal;
            query.Price = property.Price;
            query.Year = property.Year;
            query.IdOwner = property.IdOwner;

            return await _repository.Update(query);
        }
    }
}
