# Real Sate Test Dev

This test was done with C#, .NET 5, EntityFramework, SQL Server, JWT, Swagger/OpenAPI, nUnit and moq

## Getting started

To make it easy for you to get started, here's a list of recommended next steps.

### Restore Nuget Package

- In Solution explorer section in Visual Studio please select root solution
- Next, right click in root solution and select Restore Nuget Package option

### Run database migration

- Open file appsettings.json and modify DBConnection section on your sql server credentials.
```
"connectionSQLServer": "Data Source=[YOUR_SQL_SERVER_NAME];Initial Catalog=real_state_db;User Id=[YOUR_USER];Password=[YOUR_PASSWORD];"
```
- Open Package Manager Console in Tools section in Visual Studio IDE and select realstate-dev-test-backend.persistenceAdapter project
-  Run the following command

```
Update-Database
```
- That´s all!. Your database should be running succesfully!

### Build and Run Project

- The endpoints project are protected with JWT.
- By default when run migrations and deploy firstime a user admin is created to authorize endpoints
```
email: admin
password: admin
```

## Hexagonal Architecture

- realstate-dev-test-backend (Web project)
    - Controllers
    - Extension
- realstate-dev-test-backend.domainApi
    - Model
    - Port
    - Services
- realstate-dev-test-backend.persistenceAdapter
    - Config
    - Context
    - Migrations
    - Repositories
- realstate-dev-test-backend.application
- realstate-dev-test-backend.restAdapter
    - v1
        - Controllers

## Security
- JWT Authorization
